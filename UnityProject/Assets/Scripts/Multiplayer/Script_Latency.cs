﻿// Lab 06 - GAME 221 - JCCC - Prof. Tiffany Fisher
// This script fetches player latency and displays it on the player's HUD
// The script is placed on an empty game object in the Main World named Controller Scripts
//   along with a Network Identity script
// The script is also placed on the Player prefab 
//@author Linda Lane
//@date Oct. 20, 2016

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Script_Latency : NetworkBehaviour {

    NetworkClient client;
    int latency;
    Text latencyText;

    // Use this for initialization
    void Start () {
        if (isLocalPlayer)
        {
            client = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().client;
            latencyText = GameObject.Find("Latency_Text").GetComponent<Text>();
        }
        else
            Destroy(this);
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateLatency();
	}

    void UpdateLatency()
    {
        latency = client.GetRTT();

        if (latency < 200)
            latencyText.color = Color.green;
        else if (latency < 300)
            latencyText.color = Color.yellow;
        else
            latencyText.color = Color.red;

        latencyText.text = latency.ToString() + " ms";
    }
}
