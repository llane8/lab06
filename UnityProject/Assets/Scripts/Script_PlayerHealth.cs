﻿// Lab 06 - GAME 221 - JCCC - Prof. Tiffany Fisher
// This script increments and decrements player health and
// ensures the information is valid on the server and other clients
// The script is placed on the Player prefab 
//@author Linda Lane
//@date Oct. 20, 2016

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;


public class Script_PlayerHealth : NetworkBehaviour {

    //NetworkClient client;
    public const int MAX_HEALTH = 100;

    [SyncVar]
    int syncedHealth;

    public string playerName;
    public Text playerNameText;
    public const int HEALTH_INCREMENT = 5;

    public int currentHealth = MAX_HEALTH;

    // Use this for initialization
    void Start()
    {
        if (isLocalPlayer)
        {
            //client = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().client;
            playerNameText = GameObject.Find("PlayerName_Text").GetComponent<Text>();
            Debug.Log("Test health: " + currentHealth);
            //Find the button  obj
            //Get the button component
            Button damageButton = GameObject.Find("Take_Damage_Btn").GetComponent<Button>();
            //Add the function to the onClick
            damageButton.onClick.AddListener(delegate { TakeDamage(HEALTH_INCREMENT); } );
            //Find the healing button, grab the button component off of it
            Button healButton = GameObject.Find("Take_Healing_Btn").GetComponent<Button>();
            // Add the TakeHealing function on click
            healButton.onClick.AddListener(delegate { TakeHealing(HEALTH_INCREMENT); });
        }

        // Log our health upon start
        StartCoroutine(LogHealth());

        if (PlayerPrefs.HasKey("PlayerName"))
        {
            playerName = PlayerPrefs.GetString("PlayerName");
            if (playerName == "")
            {
                playerName = "Player";
            }
            playerNameText.text = playerName;
            Debug.Log(" Setting player name in prefs to: " + playerName);
        }

    }

    [Client]
    public void TakeDamage(int damageAmt)
    {
        currentHealth -= damageAmt;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }
        UpdateHealth();
    }

    [Client]
    public void TakeHealing(int healAmt)
    {
        currentHealth += healAmt;
        if (currentHealth >= 100)
        {
            currentHealth = 100;
        }
        UpdateHealth();

    }

    [Client]
    void UpdateHealth()
    {
        CmdSendHealthToServer(currentHealth);
    }

    [Command] //run on server only
    void CmdSendHealthToServer(int healthToSync)
    {
        int differenceInHealth = Mathf.Abs(currentHealth - healthToSync);
        if (differenceInHealth > HEALTH_INCREMENT)
        {
            ResetHealth();
        }
        else
            syncedHealth = healthToSync;
    }

    // Log our health 
    IEnumerator LogHealth()
    {
        Debug.Log("Player " + playerName + "'s Health is " + currentHealth + " out of " + MAX_HEALTH);
        yield return new WaitForSeconds(2);
        StartCoroutine(LogHealth());
    }

    //@ Tiffany Fisher
    [Client]
    void ResetHealth()
    {
        currentHealth = syncedHealth;
    }
}
